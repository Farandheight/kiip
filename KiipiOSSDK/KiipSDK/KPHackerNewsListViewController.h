//
//  KPHackerNewsListViewController.h
//  KiipSDK
//
//  Created by Anton Gladkov on 1/24/17.
//  Copyright © 2017 Anton Gladkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KPAPIClient;

@protocol KPHackerNewsListViewControllerDelegate

- (void)newsListViewControllerDidFinish;

@end

@interface KPHackerNewsListViewController : UITableViewController

- (instancetype)initWithAPIClient:(KPAPIClient *)apiClient
                       newsToShow:(NSInteger)numberOfNews;

@property (nonatomic, weak) id<KPHackerNewsListViewControllerDelegate> delegate;

@end

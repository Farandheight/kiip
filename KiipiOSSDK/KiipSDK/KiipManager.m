//
//  Kiip.m
//  KiipSDK
//
//  Created by Anton Gladkov on 1/25/17.
//  Copyright © 2017 Anton Gladkov. All rights reserved.
//

#import "KiipManager.h"
#import "KPHackerNewsListViewController.h"
#import "UIViewController+Modal.h"
#import "KPAPIClient.h"

@interface KiipManager () <KPHackerNewsListViewControllerDelegate>

@property (nonatomic, strong) KPAPIClient *apiClient;
@property (nonatomic, strong) UINavigationController *navController;

@end

@implementation KiipManager

+ (instancetype)sharedManager {
    static KiipManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[KiipManager alloc] init];
    });
    
    return manager;
}

- (void)initSDK {
    NSURL *baseURL = [NSURL URLWithString:@"https://hacker-news.firebaseio.com"];
    self.apiClient = [[KPAPIClient alloc] initWithBaseURL:baseURL];
}

- (void)showHackerNews {
    KPHackerNewsListViewController *controller = [[KPHackerNewsListViewController alloc] initWithAPIClient:self.apiClient
                                                                                                newsToShow:20];
    controller.delegate = self;
    self.navController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self.navController present];
}

#pragma mark - <KPHackerNewsListViewControllerDelegate>

- (void)newsListViewControllerDidFinish {
    [self.navController dismiss];
    self.navController = nil;
}

@end

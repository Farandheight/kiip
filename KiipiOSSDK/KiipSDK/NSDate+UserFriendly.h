//
//  NSDate+UserFriendly.h
//  KiipSDK
//
//  Created by Anton Gladkov on 1/26/17.
//  Copyright © 2017 Anton Gladkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSDate (UserFriendly)

- (NSString *)unitsAgoString;

@end

//
//  KPHackerNewsListCellTableViewCell.m
//  KiipSDK
//
//  Created by Anton Gladkov on 1/26/17.
//  Copyright © 2017 Anton Gladkov. All rights reserved.
//

#import "KPHackerNewsListViewCell.h"
#import "NSDate+UserFriendly.h"

@interface KPHackerNewsListViewCell ()

@property (nonatomic, weak) UILabel *titleLabel;
@property (nonatomic, weak) UILabel *authorLabel;
@property (nonatomic, weak) UILabel *dateLabel;
@property (nonatomic, weak) UILabel *scoreLabel;
@property (nonatomic, weak) UILabel *commentsLabel;

@end

@implementation KPHackerNewsListViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupSubviews];
        [self setupConstraints];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.contentView layoutIfNeeded];
    self.titleLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.titleLabel.frame);
    self.authorLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.authorLabel.frame);
    self.dateLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.dateLabel.frame);
    self.scoreLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.scoreLabel.frame);
    self.commentsLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.commentsLabel.frame);
}

- (void)setupSubviews {
    UILabel *titleLabel = [self primaryLabel];
    titleLabel.numberOfLines = 0;
    [self.contentView addSubview:titleLabel];
    self.titleLabel = titleLabel;
    
    UILabel *authorLabel = [self secondaryLabel];
    [self.contentView addSubview:authorLabel];
    self.authorLabel = authorLabel;
    
    UILabel *dateLabel = [self secondaryLabel];
    [self.contentView addSubview:dateLabel];
    self.dateLabel = dateLabel;
    
    UILabel *scoreLabel = [self secondaryLabel];
    [self.contentView addSubview:scoreLabel];
    self.scoreLabel = scoreLabel;
    
    UILabel *commentsLabel = [self secondaryLabel];
    [self.contentView addSubview:commentsLabel];
    self.commentsLabel = commentsLabel;
}

- (UILabel *)primaryLabel {
    return [self labelForFontSize:14 textColor:[UIColor darkGrayColor]];
}

- (UILabel *)secondaryLabel {
    return [self labelForFontSize:12 textColor:[UIColor lightGrayColor]];
}

- (UILabel *)labelForFontSize:(CGFloat)fontSize textColor:(UIColor *)textColor {
    UILabel *label = [[UILabel alloc] init];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.font = [UIFont systemFontOfSize:fontSize];
    label.textColor = textColor;

    return label;
}

- (void)setupConstraints {
    NSDictionary *views = NSDictionaryOfVariableBindings(_titleLabel, _authorLabel, _dateLabel, _scoreLabel, _commentsLabel);
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(10)-[_titleLabel]-(5)-[_authorLabel]-(5)-[_scoreLabel]-(10)-|"
                                                                 options:kNilOptions
                                                                 metrics:0
                                                                   views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_titleLabel]-(15)-|"
                                                                 options:kNilOptions
                                                                 metrics:0
                                                                   views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_dateLabel]-(3)-[_authorLabel]-(>=15)-|"
                                                                 options:NSLayoutFormatAlignAllBottom|NSLayoutFormatAlignAllTop
                                                                 metrics:0
                                                                   views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_scoreLabel]-(3)-[_commentsLabel]-(>=15)-|"
                                                                 options:NSLayoutFormatAlignAllBottom|NSLayoutFormatAlignAllTop
                                                                 metrics:0
                                                                   views:views]];
}

- (void)setTitle:(NSString *)title {
    self.titleLabel.text = title;
}

- (void)setAuthor:(NSString *)author {
    self.authorLabel.text = [NSString stringWithFormat:@"by %@", author];
}

- (void)setDate:(NSDate *)date {
    self.dateLabel.text = [date unitsAgoString];
}

- (void)setScore:(NSInteger)score {
    self.scoreLabel.text = [NSString stringWithFormat:@"%ld points •", (long)score];
}

- (void)setCommentsCount:(NSInteger)commentsCount {
    self.commentsLabel.text = [NSString stringWithFormat:@"%ld comments", (long)commentsCount];
}

@end

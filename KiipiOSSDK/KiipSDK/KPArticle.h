//
//  KPArticle.h
//  KiipSDK
//
//  Created by Anton Gladkov on 1/24/17.
//  Copyright © 2017 Anton Gladkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPAPIClient.h"

@interface KPArticle : NSObject

@property (nonatomic, strong) NSNumber *articleID;
@property (nonatomic, strong) NSString *author;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSNumber *countComments;
@property (nonatomic, strong) NSNumber *score;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSURL *url;
@end

@interface KPAPIClient (Articles)

- (void)getTopIDsWithCompletion:(void (^)(NSArray<NSNumber *> *, NSError *))completion;
- (void)getArticleForID:(NSNumber *)articleID completion:(void (^)(KPArticle *, NSError *))completion;

@end

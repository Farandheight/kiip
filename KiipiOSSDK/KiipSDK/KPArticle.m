//
//  KPArticle.m
//  KiipSDK
//
//  Created by Anton Gladkov on 1/24/17.
//  Copyright © 2017 Anton Gladkov. All rights reserved.
//

#import "KPArticle.h"
#import "KPArticleParser.h"

NSString * const KPTopStoriesPath = @"/v0/topstories.json";
NSString * const KPStoryDetailsPathFormat = @"/v0/item/%@.json";

@implementation KPArticle

@end

@implementation KPAPIClient (Articles)

- (void)getTopIDsWithCompletion:(void (^)(NSArray<NSNumber *> *, NSError *))completion {
    [self get:KPTopStoriesPath completion:^(NSData *data, NSError *error) {
        if (error) {
            return completion(nil, error);
        }
        
        NSArray<NSNumber *> *articleIDs = [NSJSONSerialization JSONObjectWithData:data
                                                                          options:kNilOptions
                                                                            error:&error];
        return completion(articleIDs, error);
    }];
}

- (void)getArticleForID:(NSNumber *)articleID completion:(void (^)(KPArticle *, NSError *error))completion {
    NSString *path = [NSString stringWithFormat:KPStoryDetailsPathFormat, [articleID stringValue]];
    [self get:path completion:^(NSData *data, NSError *error) {
        if (error) {
            return completion(nil, error);
        }
        
        KPArticle *article = [KPArticleParser articleFromJSONData:data
                                                            error:&error];
        completion(article, error);
    }];
}

@end

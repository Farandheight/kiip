//
//  KPApiClient.m
//  KiipSDK
//
//  Created by Anton Gladkov on 1/24/17.
//  Copyright © 2017 Anton Gladkov. All rights reserved.
//

#import "KPAPIClient.h"

NSString * const kAPIClientErrorDomain = @"com.farandheight.kiipsdk.apiclient";
NSInteger const kAPIClientRequestFailedErrorCode = 1;

@interface KPAPIClient ()

@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSURL *baseURL;

@end;

@implementation KPAPIClient

- (instancetype)initWithBaseURL:(NSURL *)baseURL {
    self = [super init];
    if (self) {
        self.baseURL = baseURL;
        self.session = [NSURLSession sharedSession];
    }
    
    return self;
}

- (void)get:(NSString *)path completion:(void (^)(NSData *, NSError *))completion {
    NSURL *fullURL = [NSURL URLWithString:path relativeToURL:self.baseURL];
    NSURLSessionDataTask *task = [self.session dataTaskWithURL:fullURL
                                             completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                 if (error) {
                                                     NSError *apiError = [self apiErrorWithCode:kAPIClientRequestFailedErrorCode
                                                                                underlyingError:error];
                                                     return completion(nil, apiError);
                                                 }
                                                 
                                                 completion(data, nil);
                                             }];
    [task resume];
}

- (NSError *)apiErrorWithCode:(NSInteger)code
              underlyingError:(NSError *)underlyingError {
    return [NSError errorWithDomain:kAPIClientErrorDomain
                               code:code
                           userInfo: @{ NSUnderlyingErrorKey : underlyingError }];
}

@end

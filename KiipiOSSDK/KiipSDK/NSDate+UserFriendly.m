//
//  NSDate+UserFriendly.m
//  KiipSDK
//
//  Created by Anton Gladkov on 1/26/17.
//  Copyright © 2017 Anton Gladkov. All rights reserved.
//

#import "NSDate+UserFriendly.h"

@implementation NSDate (UserFriendly)

- (NSString *)unitsAgoString {
    NSString *units = @"";
    NSInteger value = INT_MAX;
    NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:self];
    NSInteger minutes = interval / 60;
    if (minutes < 60) {
        units = @"minutes";
        value = minutes;
    } else if (minutes < 24 * 60) {
        units = @"hours";
        value = minutes / 60;
    } else if (minutes < 24 * 60 * 7) {
        units = @"days";
        value = minutes / (60 * 24);
    } else {
        units = @"Months";
    }
    
    return value == INT_MAX ? [NSString stringWithFormat:@"%@ ago", units] : [NSString stringWithFormat:@"%ld %@ ago", (long)value, units];
}

@end

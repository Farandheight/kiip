//
//  KPArticleParser.m
//  KiipSDK
//
//  Created by Anton Gladkov on 2/22/17.
//  Copyright © 2017 farandheight. All rights reserved.
//

#import "KPArticleParser.h"
#import "KPArticle.h"

NSString * const kParserDomain = @"com.farandheight.kiipsdk.parser";
NSInteger const kParserFailedToParseErrorCode = 1;

@implementation KPArticleParser

+ (KPArticle *)articleFromJSONData:(NSData *)data error:(NSError **)error {
    NSError *parseError;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
    if (parseError) {
        *error = [self parserErrorWithCode:kParserFailedToParseErrorCode
                           underlyingError:parseError];
        return nil;
    }
    
    KPArticle *article = [[KPArticle alloc] init];
    article.articleID = dictionary[@"id"];
    article.author = dictionary[@"by"];
    article.title = dictionary[@"title"];
    article.countComments = dictionary[@"descendants"];
    article.score = dictionary[@"score"];
    
    NSNumber *dateUnixTimestamp = dictionary[@"time"];
    article.date = dateUnixTimestamp ? [NSDate dateWithTimeIntervalSince1970:dateUnixTimestamp.integerValue] : nil;
    
    NSString *urlString = dictionary[@"url"];
    article.url = urlString ? [NSURL URLWithString:urlString] : nil;
    
    return article;
}

+ (NSError *)parserErrorWithCode:(NSInteger)code
                 underlyingError:(NSError *)underlyingError {
    return [NSError errorWithDomain:kParserDomain
                               code:code
                           userInfo: @{ NSUnderlyingErrorKey : underlyingError }];
}

@end

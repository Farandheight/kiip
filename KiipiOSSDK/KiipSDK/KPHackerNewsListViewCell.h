//
//  KPHackerNewsListCellTableViewCell.h
//  KiipSDK
//
//  Created by Anton Gladkov on 1/26/17.
//  Copyright © 2017 Anton Gladkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KPHackerNewsListViewCell : UITableViewCell

- (void)setTitle:(NSString *)title;
- (void)setAuthor:(NSString *)author;
- (void)setCommentsCount:(NSInteger)commentsCount;
- (void)setScore:(NSInteger)score;
- (void)setDate:(NSDate *)date;

@end

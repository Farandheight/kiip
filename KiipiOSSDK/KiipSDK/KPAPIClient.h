//
//  KPApiClient.h
//  KiipSDK
//
//  Created by Anton Gladkov on 1/24/17.
//  Copyright © 2017 Anton Gladkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KPAPIClient : NSObject

- (instancetype)initWithBaseURL:(NSURL *)baseURL;
- (void)get:(NSString *)path completion:(void (^)(NSData *, NSError *))completion;

@end

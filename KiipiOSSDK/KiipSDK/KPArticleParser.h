//
//  KPArticleParser.h
//  KiipSDK
//
//  Created by Anton Gladkov on 2/22/17.
//  Copyright © 2017 farandheight. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KPArticle;

@interface KPArticleParser : NSObject

+ (KPArticle *)articleFromJSONData:(NSData *)data error:(NSError **)error;

@end

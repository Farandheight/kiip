//
//  UIViewController+Modal.m
//  KiipSDK
//
//  Created by Anton Gladkov on 1/25/17.
//  Copyright © 2017 Anton Gladkov. All rights reserved.
//

#import "UIViewController+Modal.h"
#import <objc/runtime.h>

@interface KPModalPresentingController : UIViewController

@property (nonatomic, strong) UIViewController *controllerToPresent;

@end

@implementation KPModalPresentingController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.controllerToPresent) {
        [self presentViewController:self.controllerToPresent animated:YES completion:nil];
    }
}

- (void)dismissViewControllerAnimated:(BOOL)animated completion:(void (^ __nullable)(void))completion {
    self.controllerToPresent = nil;
    [super dismissViewControllerAnimated:animated completion:completion];
}

@end

@implementation UIViewController (Modal)

- (void)setHostWindow:(UIWindow *)window {
    objc_setAssociatedObject(self, @selector(hostWindow), window, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIWindow *)hostWindow {
    return objc_getAssociatedObject(self, @selector(hostWindow));
}

- (void)present {
    KPModalPresentingController *presentingController = [[KPModalPresentingController alloc] init];
    presentingController.controllerToPresent = self;
    presentingController.view.backgroundColor = [UIColor clearColor];
    
    UIWindow *window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    window.backgroundColor = [UIColor clearColor];
    window.windowLevel = UIWindowLevelAlert + 1;
    window.rootViewController = presentingController;
    [window makeKeyAndVisible];
    self.hostWindow = window;
}

- (void)dismiss {
    [self.hostWindow.rootViewController dismissViewControllerAnimated:YES completion:nil];
    [self.hostWindow resignKeyWindow];
    self.hostWindow = nil;
}

@end

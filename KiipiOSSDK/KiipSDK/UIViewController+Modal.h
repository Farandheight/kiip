//
//  UIViewController+Modal.h
//  KiipSDK
//
//  Created by Anton Gladkov on 1/25/17.
//  Copyright © 2017 Anton Gladkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Modal)

- (void)present;
- (void)dismiss;

@end

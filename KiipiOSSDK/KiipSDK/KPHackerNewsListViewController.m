//
//  KPHackerNewsListViewController.m
//  KiipSDK
//
//  Created by Anton Gladkov on 1/24/17.
//  Copyright © 2017 Anton Gladkov. All rights reserved.
//

#import "KPHackerNewsListViewController.h"
#import "KPAPIClient.h"
#import "KPArticle.h"
#import "KPWebViewController.h"
#import "KPHackerNewsListViewCell.h"
#import "KiipManager.h"

NSString * const KPHackerNewsCellIdentifier = @"KPHackerNewsCellIdentifier";

@interface KPHackerNewsListViewController ()

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) NSCache<NSNumber *, KPArticle *> *articlesCache;
@property (nonatomic, strong) NSCache<NSNumber *, NSNumber *> *rowHeightsCache;
@property (nonatomic, strong) NSArray<NSNumber *> *topIDs;
@property (nonatomic, strong) KPAPIClient *apiClient;
@property (nonatomic, strong) KPHackerNewsListViewCell *prototypeCell;
@property (nonatomic, assign) NSInteger numberOfNewsToShow;

@end

@implementation KPHackerNewsListViewController

- (instancetype)initWithAPIClient:(KPAPIClient *)apiClient
                       newsToShow:(NSInteger)newsToShow {
    self = [super init];
    if (self) {
        self.apiClient = apiClient;
        self.numberOfNewsToShow = newsToShow;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Top";
    self.articlesCache = [[NSCache alloc] init];
    self.articlesCache.countLimit = self.numberOfNewsToShow;
    self.rowHeightsCache = [[NSCache alloc] init];
    self.rowHeightsCache.countLimit = self.numberOfNewsToShow;
    [self.tableView registerClass:[KPHackerNewsListViewCell class] forCellReuseIdentifier:KPHackerNewsCellIdentifier];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(onDoneButtonTapped:)];
    self.navigationItem.rightBarButtonItem = doneButton;
    [self loadArticles];
}

#pragma mark - <UITableViewDataSource>

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.numberOfNewsToShow;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KPHackerNewsListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:KPHackerNewsCellIdentifier forIndexPath:indexPath];
    NSNumber *articleID = self.topIDs[indexPath.row];
    KPArticle *cachedArticle = [self.articlesCache objectForKey:articleID];
    if (cachedArticle) {
        [self configureCell:cell forArticle:cachedArticle];
    }
    
    return cell;
}

#pragma mark - <UITableViewDelegate>

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *articleID = self.topIDs[indexPath.row];
    KPArticle *article = [self.articlesCache objectForKey:articleID];
    UIViewController *controller = [[KPWebViewController alloc] initWithURL:article.url];
    controller.title = article.title;
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *cachedHeight = [self.rowHeightsCache objectForKey:@(indexPath.row)];
    if (cachedHeight) {
        return cachedHeight.floatValue;
    }
    
    NSNumber *articleID = self.topIDs[indexPath.row];
    KPArticle *article = [self.articlesCache objectForKey:articleID];
    if (!article) {
        return 100;
    }
    
    CGFloat calculatedHeight = [self heightForCellWithArticle:article];
    [self.rowHeightsCache setObject:@(calculatedHeight) forKey:@(indexPath.row)];
    
    return calculatedHeight;
}

#pragma mark - Private Methods

- (CGFloat)heightForCellWithArticle:(KPArticle *)article {
    if (!self.prototypeCell) {
        self.prototypeCell = [[KPHackerNewsListViewCell alloc] initWithFrame:self.view.frame];
    }
    
    [self configureCell:self.prototypeCell forArticle:article];
    [self.prototypeCell setNeedsLayout];
    [self.prototypeCell layoutIfNeeded];
    CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    return size.height + 1;
}

- (void)configureCell:(KPHackerNewsListViewCell *)cell forArticle:(KPArticle *)article {
    [cell setTitle:article.title];
    [cell setAuthor:article.author];
    [cell setCommentsCount:article.countComments.integerValue];
    [cell setScore:article.score.integerValue];
    [cell setDate:article.date];
}

- (void)onDoneButtonTapped:(UIButton *)button {
    [self.delegate newsListViewControllerDidFinish];
}

- (void)loadArticles {
    [self.apiClient getTopIDsWithCompletion:^(NSArray<NSNumber *> *topIDs, NSError *error) {
        if (error) {
            return;
        }
        
        self.topIDs = topIDs;
        [self loadArticlesForIDs:topIDs];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            self.title = [NSString stringWithFormat:@"Top %ld", (long)self.numberOfNewsToShow];
        });
    }];
}

- (void)loadArticlesForIDs:(NSArray<NSNumber *> *)ids {
    for (int index = 0; index < self.numberOfNewsToShow; index++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        NSNumber *articleID = ids[index];
        
        [self.apiClient getArticleForID:articleID completion:^(KPArticle *article, NSError *error) {
            if (error) {
                return;
            }
            
            [self.articlesCache setObject:article forKey:articleID];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            });
        }];
    }
}

- (NSInteger)numberOfNewsToShow {
    return MIN(self.topIDs.count, _numberOfNewsToShow);
}

@end

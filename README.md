## Implementation of Kiip's take-home challenge
### Prerequisites
 - Cocoapods 1.0.1
 - Xcode 7
 - Cordova (https://cordova.apache.org/)

### Directory Structure
  - `KiipiOSSDK` - iOS Project for Kiip SDK
  - `KiipCordovaPlugin` - Cordova Plugin Project
  - `scripts` - contains convenience bash scripts for building SDK, Cordova plugin and Cordova application

### Installation
- Run `script/build_cordova_plugin`.  This will do the following:
    1. Get the Cocoapods dependencies for the `KiipSDK` project
  2. Build the `KiipSDK` static library
  3. Create a Universal `KiipSDK` static framework
  4. Copy the framework into the directory of the Cordova plugin, i.e. `KiipCordovaPlugin/src/ios`.

- Run `script/create_test_app`.  This will do the following
    1. Create a new Cordova application named `KiipCordovaTestApp`
  2. Add `ios` platform to the application
  3. Install the `KiipCordovaPlugin` plugin into the application.
  4. Add the following code to the `index.js` which will cause the Hacker News feed to show as soon as the application is loaded:
  ``` js
  var kiip = cordova.require('com.farandheight.kiip.KiipCordovaPlugin');
  kiip.initSDK();
  kiip.showHackerNews();
  ```
- Open `HelloCordova.workspace` in Xcode (located under `KiipCordovaTestApp/platforms/ios`) and run the app
     
    *Note: Appropriate Bundle ID/Provisioning profile will need to be configured in order to run on a physical device.
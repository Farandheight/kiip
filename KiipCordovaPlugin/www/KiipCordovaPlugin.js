var exec = require('cordova/exec');

// Currently no fail scenarios, so not allowing success/failure callbacks.
exports.initSDK = function() {
    exec(null, null, "KiipCordovaPlugin", "initSDK", []);
};

exports.showHackerNews = function() {
	exec(null, null, "KiipCordovaPlugin", "showHackerNews", []);
}

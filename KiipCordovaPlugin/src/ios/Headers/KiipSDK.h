//
//  KiipSDK.h
//  KiipSDK
//
//  Created by Farandheight on 1/24/17.
//  Copyright © 2017 farandheight. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for KiipSDK.
FOUNDATION_EXPORT double KiipSDKVersionNumber;

//! Project version string for KiipSDK.
FOUNDATION_EXPORT const unsigned char KiipSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <KiipSDK/PublicHeader.h>

#import <KiipSDK/KiipManager.h>

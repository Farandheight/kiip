//
//  Kiip.h
//  KiipSDK
//
//  Created by Farandheight on 1/25/17.
//  Copyright © 2017 farandheight. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KiipManager : NSObject

+ (instancetype)sharedManager;

- (void)initSDK;
- (void)showHackerNews;

@end

/********* KiipCordovaPlugin.m Cordova Plugin Implementation *******/

#import "KiipCordovaPlugin.h"
#import <KiipSDK/KiipSDK.h>

@implementation KiipCordovaPlugin

- (void)initSDK:(CDVInvokedUrlCommand*)command {
    [[KiipManager sharedManager] initSDK];
}

- (void)showHackerNews:(CDVInvokedUrlCommand *)command {
    [[KiipManager sharedManager] showHackerNews];
}

@end

/********* KiipCordovaPlugin.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>

@interface KiipCordovaPlugin : CDVPlugin

- (void)initSDK:(CDVInvokedUrlCommand *)command;
- (void)showHackerNews:(CDVInvokedUrlCommand *)command;

@end
